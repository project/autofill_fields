<?php

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function autofill_fields_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state) {
  // Skip some fields.
  if (in_array($field_definition->getName(), ['authored_by', 'authored_on', 'title', 'path', 'comment',])) {
    return [];
  }
  $element = [
    'autofill' => [
      '#type' => 'checkbox',
      '#title' => t('Autofill field'),
      '#description' => t('Autofill field for new entities with last used value.'),
      '#default_value' => $plugin->getThirdPartySetting('autofill_fields', 'autofill', FALSE),
    ],
  ];
  return $element;
}

/**
 * Implements hook_field_widget_settings_summary_alter().
*/
function autofill_fields_field_widget_settings_summary_alter(&$summary, $context) {
  if ($context['widget']->getThirdPartySetting('autofill_fields', 'autofill')) {
    $summary[] = t('Autofill enabled.');
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function autofill_fields_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  // Only run for logged in users.
  if (!$user_id = \Drupal::currentUser()->id()) {
    return;
  }
  $entity = $form_state->getFormObject()->getEntity();
  // Only run for new entities; we don't want to overwrite desired data.
  if (!$entity->isNew()) {
    return;
  }
  $plugin = $context['widget'];

  // Check that it is enabled for this field.
  if (!$plugin->getThirdPartySetting('autofill_fields', 'autofill')){
    return;
  }
  $field_definition = $context['items']->getFieldDefinition();
  $type = $field_definition->getType();
  $plugin_id = $plugin->getPluginId();

  $values = $plugin->getThirdPartySetting('autofill_fields', 'stored', []);
  if (isset($values[$user_id])) {
    $entity_type_manager = \Drupal::service('entity_type.manager');
    if ($type == 'text_with_summary') {
      $element['#default_value'] = $values[$user_id][0]['value'];
      $element['#format'] = $values[$user_id][0]['format'];
      $element['summary']['#default_value'] = $values[$user_id][0]['summary'];
    }
    elseif($type == 'entity_reference') {
      $value = isset($values[$user_id]['target_id']) ? $values[$user_id]['target_id'] : $values[$user_id];
      if ($plugin_id == 'options_select' || $plugin_id == 'options_buttons') {
        $element['#default_value'] = [];
        foreach($value as $reference) {
          $element['#default_value'][] = $reference['target_id'];
        }
      }
      elseif ($plugin_id == 'entity_reference_autocomplete') {
        if (!$value[$element['target_id']['#delta']]['target_id']) {
          return;
        }
        $element['target_id']['#default_value'] = $entity_type_manager
          ->getStorage($element['target_id']['#target_type'])
          ->load($value[$element['target_id']['#delta']]['target_id']);
      }
      elseif($plugin_id == 'entity_reference_autocomplete_tags') {
        $element['target_id']['#default_value'] = [];
        foreach($value as $reference) {
          $element['target_id']['#default_value'][] = $entity_type_manager
            ->getStorage($element['target_id']['#target_type'])
            ->load($reference['target_id']);
        }
      }
    }
    elseif ($type == 'decimal') {
      if ($plugin_id == 'number') {
        $element['value']['#default_value'] = $values[$user_id][$element['value']['#delta']]['value'];
      }
    }
    elseif ($type == 'datetime') {
      if ($plugin_id == 'datetime_default') {
        $element['value']['#default_value'] = new DrupalDateTime($values[$user_id]);
      }
    }
    else {
      var_dump($type);
      var_dump($values[$user_id]);
      var_dump($element);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function autofill_fields_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $info = $form_state->getBuildInfo();
  if (isset($info['base_form_id']) && in_array($info['base_form_id'], ['node_form', 'media_form'])) {
    if (isset($form['actions']['publish'])) {
      $form['actions']['publish']['#submit'][] = 'autofill_fields_submit';
    }
    else {
      $form['actions']['submit']['#submit'][] = 'autofill_fields_submit';
    }
  }
}

/**
 * Submit function to save field values per entity create/edit action.
 */
function autofill_fields_submit($form, FormStateInterface $form_state) {
  if (!$user_id = \Drupal::currentUser()->id()) {
    return;
  }

  // Ensure that the form entity is properly loaded.
  if (!$entity = $form_state->getFormObject()->getEntity()) {
    return;
  }

  //TODO: get real form mode.
  $form_mode = 'default';
  $entity_form_display = EntityFormDisplay::load($entity->getEntityTypeId() . '.' . $entity->getType() . '.' . $form_mode);
  foreach ($entity_form_display->getComponents() as $name => $component) {
    if (empty($component['third_party_settings']['autofill_fields']['autofill'])) {
      continue;
    }
    if (!isset($component['third_party_settings']['autofill_fields']['stored'])) {
      $component['third_party_settings']['autofill_fields']['stored'] = [];
    }
    $component['third_party_settings']['autofill_fields']['stored'][$user_id] = $form_state->getValue($name);
    if ($component['type'] == 'datetime_default') {
      $value = $form_state->getValue($name)[0]['value']->format('c');
    }
    else {
      $value = $form_state->getValue($name);
    }
    $component['third_party_settings']['autofill_fields']['stored'][$user_id] = $value;
    $entity_form_display->setComponent($name, $component);
  }
  $entity_form_display->save();

}
