Autofill Fields
===============

Autofill Fields provides the ability to remember last used values, on a per field instance basis, and automatically populate the field next time it is used. Certainly this is not very good for many content types and circumstances but there are times that this can be a massive time saver. Two use cases in which it could be very effective would be:
  <ul>
    <li>Drupal Commerce: adding many products with similar attributes. For example, adding 30 different distinct products which all have the same price; enter the price once and it'll remember it all the way through.</li>
    <li>BriarMoon Accounting: The application/module family it was developed for. Unreleased.</li>
  </ul>

##REQUIREMENTS

Drupal 8 is required, Drupal 8.1.x or higher is suggested.

##INSTALLATION
Install as you would normally install a contributed Drupal module. See the <a href='http://drupal.org/documentation/install/modules-themes/modules-8'>Drupal 8 instructions </a> if required in the Drupal documentation for further information.

##CONFIGURATION
Configuration can be accessed per field instance wherever that field instance is usually configured. For example at for the Node type Article that would be at /admin/structure/types/manage/article/form-display.

##FAQ
Any questions? Ask away on the issue queue or email: design@briarmoon.ca.

This project has been sponsored by:
###<a href="http://design.briarmoon.ca">BriarMoon Design</a>
   Full service web development and design studio. Specializing in responsive, secure, optimized Drupal sites. BriarMoon Design can help you with all your Drupal needs including installation, module creation or debugging, themeing, customization, and hosting.